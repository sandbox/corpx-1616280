<?php
// $Id: cxdots.admin.inc,v 1.2 2011/02/15 22:35:07 dustincurrie Exp $

/**
 * Menu callback for cxdots settings.
 */

function cxdots_settings() {
  $form = array();

  $form['cxdots_contextual_links_enabled'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Contextual Links Override'),
    '#description'   =>t('Enable this function.  Off by default'),
    '#default_value' => variable_get('cxdots_contextual_links_enabled'),
  );

  $form['cxdots_sorting_enabled'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Manual Sorting Icon Override'),
    '#description'   =>t('Enable this function.  Off by default'),
    '#default_value' => variable_get('cxdots_sorting_enabled'),
  );

  $form['cxdots_summary_enabled'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Summary TextArea Override'),
    '#description'   =>t('Enable this function.  Off by default'),
    '#default_value' => variable_get('cxdots_summary_enabled'),
  );

  $form['cxdots_addanother_enabled'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Enable Add Another Override'),
    '#description'   =>t('Enable this function.  Off by default'),
    '#default_value' => variable_get('cxdots_addanother_enabled'),
  );

  $form['cxdots_deleteline_enabled'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Enable Deleteline Override'),
    '#description'   =>t('Enable this function.  Off by default'),
    '#default_value' => variable_get('cxdots_deleteline_enabled'),
  );


  $form['cxdots_deleteline_enabled'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Enable Deleteline Override'),
    '#description'   =>t('Enable this function.  Off by default'),
    '#default_value' => variable_get('cxdots_deleteline_enabled'),
  );


  return system_settings_form($form);
}
